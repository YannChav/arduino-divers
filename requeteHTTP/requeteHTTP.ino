//Permet d'inclure les bibliothèques nécessaires au fonctionnement de l'envoi de la requête
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

//Définit les variables de connexion
const char ssid[] = "your_ssid";
const char password[] = "your_password";

//Définit l'adresse IP du serveur (ici de notre Raspberry Pi)
byte server[] = {192, 168, 185, 251};

//Client pour effectuer les requêtes HTTP
WiFiClient client;

void setup() {
  //Initialise le terminal d'exécution
  Serial.begin(115200);

  //Se connecte au réseau Wifi
  WiFi.begin(ssid, password);
  Serial.println("Connexion en cours");

  //Affiche . tant que le réseau n'est pas connecté
  while(WiFi.status() != WL_CONNECTED) { 
    delay(500);
    Serial.print(".");
  }

  //Affiche l'adresse IP utilisé par l'ESP8266
  Serial.println("");
  Serial.print("Connecté au réseau Wifi avec l'adresse IP: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  //Vérifie le statut de la connexion
  if(WiFi.status()== WL_CONNECTED){
      //Si le client arrive à se connecter au serveur
      if(client.connect(server, 80)){
        //Envoie la requête au serveur
        Serial.println("Requête envoyée");
        client.println("GET /assets/api-station/v1/add-release.php?sensor=1&temperature=23&humidity=42 HTTP/1.0");
        client.println(); 
        //Ferme la connexion
        client.stop();
      //Sinon, un message d'erreur est affiché
      }else{
        Serial.println("ERREUR: Connexion au serveur (Raspberry Pi) impossible");
      }
  //Si la connexion ne fonctionne pas
  }else {
    Serial.println("WiFi non connecté");
  }

  //Envoi une requête toutes les 30 secondes
  delay(30000);  
}
