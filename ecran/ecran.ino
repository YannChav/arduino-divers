#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_I2CDevice.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define LOGO_HUM_HEIGHT   13
#define LOGO_HUM_WIDTH    13

#define LOGO_TEMP_HEIGHT   15
#define LOGO_TEMP_WIDTH    15

#define LOGO_RES_HEIGHT   12
#define LOGO_RES_WIDTH    12

static const unsigned char PROGMEM logo_humidity[] =
//Logo représentant une goute d'eau (pour le taux d'humidité)
 { 0b00000000, 0b01000000,
   0b00000001, 0b10000000,
   0b00000011, 0b10000000,
   0b00000111, 0b11000000,
   0b00001111, 0b11000000,
   0b00001111, 0b11100000,
   0b00011111, 0b11110000,
   0b00011111, 0b11110000,
   0b00011111, 0b11110000,
   0b00011101, 0b11110000,
   0b00001001, 0b11110000,
   0b00000100, 0b11100000,
   0b00000011, 0b11000000,
 };
static const unsigned char PROGMEM logo_temperature[] =
//Logo représentant un thermomètre (pour la température)
 {
   0b00000001, 0b10000000,
   0b00000001, 0b10110000,
   0b00000001, 0b10000000,
   0b00000001, 0b10100000,
   0b00000001, 0b10000000,
   0b00000001, 0b10110000,
   0b00000001, 0b10000000,
   0b00000001, 0b10100000,
   0b00000001, 0b10000000,
   0b00000001, 0b10110000,
   0b00000011, 0b11000000,
   0b00000111, 0b11100000,
   0b00000111, 0b11100000,
   0b00000011, 0b11000000,
   0b00000001, 0b10000000,
 };
static const unsigned char PROGMEM logo_reseau[] =
//Logo représentant les barres réseau
 {
   0b00000000, 0b00000111,
   0b00000000, 0b00000111,
   0b00000000, 0b00000111,
   0b00000000, 0b00000111,
   0b00000001, 0b11000111,
   0b00000001, 0b11000111,
   0b00000001, 0b11000111,
   0b00000001, 0b11000111,
   0b01110001, 0b11000111,
   0b01110001, 0b11000111,
   0b01110001, 0b11000111,
   0b01110001, 0b11000111,
 };

//Déclare la fonction que nous allons utiliser pour afficher des informations à l'écran
void  ecranOLED();

void setup() {
  Wire.begin(2,0);
  Serial.begin(9600);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  //Affiche l'image d'accueil d'Adafruit
  display.display();
  delay(2000);
  //Nettoie l'écran
  display.clearDisplay();
  // Draw a single pixel in white
  display.drawPixel(10, 10, SSD1306_WHITE);
  // Show the display buffer on the screen. You MUST call display() after
  // drawing commands to make them visible on screen!
  display.display();
  delay(2000);

  ecranOLED();
}

void loop() {
}

void ecranOLED(void) {
  display.clearDisplay();

  display.setTextSize(1); 
  display.setTextColor(SSD1306_WHITE);

  //Affiche les informations relatives à la sonde
  display.setCursor(36, 0);
  display.println(F("ID SONDE : 1"));

  //Affiche les logos que nous allons utiliser
  display.drawBitmap(0, 16, logo_humidity, LOGO_HUM_WIDTH, LOGO_HUM_HEIGHT, SSD1306_WHITE);
  display.drawBitmap(0, 34, logo_temperature, LOGO_TEMP_WIDTH, LOGO_TEMP_HEIGHT, SSD1306_WHITE);
  display.drawBitmap(0, 50, logo_reseau, LOGO_RES_WIDTH, LOGO_RES_HEIGHT, SSD1306_WHITE);

  float hum = 43.2;
  //Affiche les informations relatives à l'humidité
  display.setCursor(18, 16);
  display.print(F("Humidite : "));
  display.print(hum);
  display.println(F("%"));

  double temp = 25;
  //Affiche les informations relatives à la température
  display.setCursor(18, 34);
  display.print(F("Temp : "));
  display.print(temp);
  display.println(F(" C"));

  //Affiche les informations relatives au réseau
  display.setCursor(18, 50);
  display.println(F("192.168.185.243/24"));
  
  //Affiche ce que nous lui demandons
  display.display();
}
